﻿//шифр цезаря

#include <iostream>
#include <string.h>

using namespace std;

char * fol(int d, char * array)
{
	for (int i = 0; i < strlen(array); i++)
	{
		if (array[i] != 32)
		{
			if ((64) < array[i] + d < 123)
			{
				array[i] += d;
			}
			else if (array[i] + d > 122)
			{
				array[i] = (((array[i] + d) % 122) + 64);
			}
		}
	}

	return array;

}

int main()
{
	char A[] = "ABCDE";
	fol(1, A);

	//вывод массива
	cout << "\n";
	for (int i = 0; i < strlen(A); i++)
	{
		cout << A[i];
	}
	cout << "\n";
	getchar();
	return 0;
}
