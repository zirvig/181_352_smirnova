#include "pch.h"
#include <iostream>
#include <string>

using namespace std;

//*node - чтобы переменная оставалась когда выключается функция 

//расписывание переменных 
class Tree {
private:
	char Data; // куда сохраняется скобки
	Tree *LeftTree; // левый потомок
	Tree *RightTree; // правый потомок
//расписание функций
public:
	void addtree(char data, Tree *&node); // Функция добавления узла к дереву
	void print(Tree *node); // в этой функции происходит сравнение и вывод скобок
	void FreeTree(Tree *node); // Функция для удаление правого и левого поддерева
};


// Функция добавления узла к дереву
void Tree::addtree(char data, Tree *&node)
{
	if (!node) //Пока не встретится пустой корень дерева 
	{
		node = new Tree; //Выделяем память
		node->Data = data; //Кладем в выделенное место Data
		node->LeftTree = 0; // Добавляем к корню дерева левую сторону
		node->RightTree = 0; // Добавляем к корню дерева правую сторону
		return;
	}
	else
		if (node->Data > data)
			addtree(data, node->LeftTree); //Рекурсивная функция для левого поддерева
		else
			addtree(data, node->RightTree); //Рекурсивная функция для правого поддерева

}

int kv, kr, fig; 
bool flag = true; //принимает значение тру и фолз (тру==1)

void Tree::print(Tree *node)
{
	if (!node) return; // Выход при отсутствии ветки

	 

	if (node->Data == '[')
	{
		kv++;
		cout << kv;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == ']')
	{
		if (kv == 0) flag = false;
		kv--;
		cout << kv;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == '{')
	{
		fig++;
		cout << fig;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == '}')
	{
		if (fig == 0) flag = false;
		fig--;
		cout << fig;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == '(')
	{
		kr++;
		cout << kr;
		cout << node->Data << endl; // Данные ветки
	}
	else if (node->Data == ')')
	{
		if (kr == 0) flag = false;
		kr--;
		cout << kr;
		cout << node->Data << endl; // Данные ветки
	}

	print(node->LeftTree); // Выводим ветки те, что находятся слева
	print(node->RightTree); //Выводим ветки те, что находятся справа

	return;
}
void Tree::FreeTree(Tree *node) // Удаление правого и левого поддерева
{
	if (!node) return; // Выход при отсутствии ветки
	FreeTree(node->LeftTree);
	FreeTree(node->RightTree);
	delete node;
	return;
}

int main() {
	setlocale(LC_ALL, "RUS");
	Tree *Node = NULL; // Инициализация ноды пустотой
	Tree b;
	string a;
	cout << "Введите скобки: " << endl;
	getline(cin, a);

	for (int i = 0; i < a[i]; i++)
	{
		b.addtree(a[i], Node);
	}

	cout << "-------- " << endl;
	cout << "Сортировка: " << endl;
	b.print(Node); // вывод узла
	b.FreeTree(Node); // удаление узла

	cout << "------- " << endl;
	cout << "Результат: " << endl;
	if ((kv != 0 || kr != 0 || fig != 0) || (flag == false))
	{
		cout << "[Не верно]" << endl;
	}
	else
	{
		cout << "[Верно]" << endl;
	}
	cout << endl;

	system("pause");
	return 0;
}
