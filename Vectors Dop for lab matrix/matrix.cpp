#include <iostream>
#include "matrix.h"
//#include "vector.h"
using namespace std;

matrix::matrix() // ����������� �� ��������� � ���������� ���������
{
	rows = 0;
	columns = 0;

	matr = new double*[rows];
	for (int i = 0; i < rows; i++) {
		matr[i] = new double[columns];
	}
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++)
		{
			matr[i][j] = 0;
		}
	}
}

matrix::matrix(int m, int n) // ����������� � �����������
{

	rows = m;
	columns = n;

	matr = new double*[rows];
	for (int i = 0; i < rows; i++) {
		matr[i] = new double[columns];
	}
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++)
		{
			matr[i][j] = 0;
		}
	}
}

//matrix::matrix(double **(&ptr), matrix & a) // ����������� ����������� 
//{
//	this->rows = a.rows;
//	this->columns = a.columns;
//
//	this->matr = new double*[this->rows];
//	for (int i = 0; i < this->rows; i++) {
//		matr[i] = new double[this->columns];
//	}
//
//	for (int i = 0; i < this->rows; i++) {
//		for (int j = 0; j < this->columns; j++) {
//			this->matr[i][j] = a.matr[i][j];
//		}
//	}
//
//	ptr = new double*[this->rows];
//	for (int i = 0; i < this->rows; i++)
//		ptr[i] = new double[this->columns];
//
//	//srand(time(NULL)); // ����������� ������������� �� �����
//	for (int i = 0; i < this->rows; i++) {
//		for (int j = 0; j < this->columns; j++) {
//			ptr[i][j] = rand() % 21 - 10;
//		}
//	}
//
//	for (int i = 0; i < this->rows; i++) {
//		for (int j = 0; j < this->columns; j++) {
//			matr[i][j] = ptr[i][j];
//		}
//	}
//}


matrix::matrix(double **ptr, int m, int n): rows(m), columns(n)
{
	matr = new double*[m];
	for (int i = 0; i < m; i++) {
		matr[i] = new double[n];
	}

	ptr = new double*[m];
		for (int i = 0; i < m; i++)
			ptr[i] = new double[n];
	
		//srand(time(NULL)); // ����������� ������������� �� �����
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				ptr[i][j] = rand() % 21 - 10;
			}
		}
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++)
		{
			matr[i][j] = ptr[i][j];
		}
	}
}

matrix::matrix(const matrix& a)
{
	rows = a.rows;
	columns = a.columns;

	matr = new double*[rows];
	for (int i = 0; i < rows; i++) {
		matr[i] = new double[columns];
	}

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			matr[i][j] = a.matr[i][j];
		}
	}
}

matrix::~matrix() // ���������� ������ 
	{
	for (int i = 0; i < rows; i++) {
		delete[] matr[i];
	}
		delete[] matr;
	}


//void matrix::get_elements() // ���� ��������� �������
//{
//	for (int i = 0; i < rows; i++)
//	{
//		for (int j = 0; j < columns; j++)
//		{
//			cout << "������� [" << i << "]" << "[" << j << "] �������: ";
//			cin >> matr[i][j];
//		}
//	}
//}

void matrix::show_matr() // ����� ������� �� �����
{
	for (int i = 0; i < rows; i++) {
		cout << endl;
		for (int j = 0; j < columns; j++) {
			cout << "\t" << matr[i][j] << " ";
		}
	}
	cout << endl;
}

matrix &matrix::operator = (const matrix & a) // ������������� �������� ����������
{
	rows = a.rows;
	columns = a.columns;

	matr = new double*[rows];
	for (int i = 0; i < rows; i++) {
		matr[i] = new double[columns];
	}

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			matr[i][j] = a.matr[i][j];
		}
	}
	//	rows = a.rows;
	//	columns = a.columns;
	//	matr = new double *[a.rows];
	//	for (int i = 0; i < a.rows; i++) {
	//		matr[i] = new double[a.columns];
	//	}
	//
	//for (int i = 0; i < a.rows; i++) {
	//	for (int j = 0; j < a.columns; j++) {
	//		matr[i][j] = a.matr[i][j];
	//	}
	//}

	return *this;
}

matrix operator+ (matrix & a, matrix & b) // ������������� ������������� �������� ��������
{
	if (a.rows == b.rows && a.columns == b.columns) {

		/*for (int i = 0; i < *a.rows; i++) {
			for (int j = 0; j < *a.columns; j++) {
				a.matr[i][j] = a.matr[i][j] + b.matr[i][j];
			}
		}*/
		matrix rez(a.rows, a.columns);
		for (int i = 0; i < a.rows; i++) {
			for (int j = 0; j < a.columns; j++) {
				rez.matr[i][j] = 0;
				rez.matr[i][j] = a.matr[i][j] + b.matr[i][j];
			}
		}

		return rez;

	}
	
	//return a;
}

matrix operator* (matrix & a, matrix & b) // ������������� ������������� �������� ���������
{
	if (a.rows == b.columns) {
		//double ** rez;
		//rez = new double*[*a.rows];
		//for (int i = 0; i < *a.rows; i++) {
		//	rez[i] = new double[*b.columns];
		//}
		////matrix rez(a.rows, b.columns);
		//for (int i = 0; i < *a.rows; i++) {
		//	for (int j = 0; j < *b.columns; j++) {
		//		rez[i][j] = 0;
		//	}
		//}
		//for (int i = 0; i < *a.rows; i++) {
		//	for (int j = 0; j < *b.columns; j++) {
		//		for (int k = 0; k < *b.columns; k++) {
		//			rez[i][j] += a.matr[i][k] * b.matr[k][j]; // ��� �� ��������
		//		}
		//	}
		//}

		//for (int i = 0; i < *a.rows; i++) {
		//	for (int j = 0; j < *b.columns; j++) {
		//		a.matr[i][j] = rez[i][j];
		//	}
		//}

		//for (int i = 0; i < *a.rows; i++) {
		//	delete[] rez[i];
		//}
		//delete[] rez;

		//return a;

	/*	rows = new int;
		columns = new int;

		rows = a.rows;
		columns = a.columns;
*/
		matrix rez(a.rows, b.columns);
		for (int i = 0; i < a.rows; i++) {
			for (int j = 0; j < b.columns; j++) {
				rez.matr[i][j] = 0;
				for (int k = 0; k < b.columns; k++) {
					rez.matr[i][j] += a.matr[i][k] * b.matr[k][j];
				}
			}
		}
		return rez;

	}
}

