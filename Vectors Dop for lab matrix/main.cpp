#include <iostream>
#include "matrix.h"
#include "vector.h"
using namespace std;


int main()
{
	setlocale(LC_ALL, "rus");

	int m = 3, n = 3;
	double **arr = { 0 };

	// ������ ������

	//std::cout << "������� ����� ����� ������: ";
	//cin >> m;
	//std::cout << "������� ����� �������� ������: ";
	//cin >> n;
	//std::cout << endl;

	//matrix size_matr(m, n);
	matrix first_matr(arr, m, n), second_matr(arr, m, n);

	std::cout << "	���� ������� �1: " << endl;
	first_matr.show_matr();
	std::cout << endl;

	std::cout << "	���� ������� �2: " << endl;
	second_matr.show_matr();
	std::cout << endl;

	// �������� � ���������

	matrix third_matr(m, n), fourth_matr(m,n);

	third_matr = first_matr + second_matr;
	std::cout << "	��������� �������� ������: " << endl;
	third_matr.show_matr();
	std::cout << endl;

	fourth_matr = first_matr * second_matr;
	std::cout << "	��������� ��������� ������: " << endl;
	fourth_matr.show_matr();
	std::cout << endl;

	int size;

	// ������ ��������

	std::cout << "������� ���������� ��������� �������: ";
	cin >> size;
	std::cout << endl;

	vectorV first_vctr(arr, 1, size), second_vctr(arr, 1, size);

	std::cout << "	��� ������ �1: " << endl << endl;
	first_vctr.show_matr();
	std::cout << endl << endl;
	std::cout << "	��� ������ �2: " << endl << endl;
	second_vctr.show_matr();
	std::cout << endl << endl;

	// �������� � ���������
	
	std::cout << "	��������� ���������� ��������� ��������: " << endl << endl;

	first_vctr.cross(second_vctr);


	getchar();
	getchar();
	return 0;
};