#pragma once

class vectorV;
class matrix
{
protected:
	double **matr;
	int rows, columns;

public:

	matrix();
	matrix(int m, int n);
	//matrix(double **(&ptr), matrix & a);
	matrix(double **ptr, int m, int n);
	matrix(const matrix& a);
	~matrix();

	//void get_elements(); 
	void show_matr();

	matrix &operator= (const matrix & a); 
	friend matrix operator+ (matrix & a, matrix & b);
	friend matrix operator* (matrix & a, matrix & b);
};