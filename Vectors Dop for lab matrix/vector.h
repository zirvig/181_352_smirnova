#pragma once
#include "matrix.h"
class matrix;

//class vector
//{
//	double *vctr = nullptr;
//	int rows = 0, columns = 0;
//	friend matrix;
//public:
//	vector();
//	vector(int x);
//	vector(double *ptr, int x);
//	~vector();
//	friend matrix;
//
//	void get_elements();
//	void show_vec();
//
//	friend vector operator * (const vector & a, const vector & b);
//	friend matrix operator* (const matrix & a, const vector & b);
//};

class vectorV : public matrix
{
private:
	//double **vctr;
	//int rowsV;
	//int columnsV;
	int resV = 0;

public:

	//vector();
	//vector(int m, int n);
	//vector(double **ptr, int m, int n);
	//~vector();
	//void get_elements();
	//void show_vec();

	vectorV(double **ptr, int m, int n) : matrix(ptr, m, n) {}

	//void multScalar();
	void cross(const vectorV & a);
};
