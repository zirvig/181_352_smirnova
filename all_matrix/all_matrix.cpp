﻿#include "pch.h"
#include <iostream>

using namespace std;

int main() {
	int num = 0;
	cout << "\nViberete razdel programmi" << endl; //вывод выбора программ построчно
	cout << "\n1 - poelementnoe proizvedenie matrix";
	cout << "\n2 - proizvedenie matrix na matrix";
	cout << "\n3 - proizvedenie matrix na vector";
	cout << "\n4 - skalyarnoe proizvedenie vectorov";
	cin >> num;
	if (num == 1) { //вывод нужной проги
		int x, y = 0;
		int a[3][3], b[3][3], c[3][3]; //объявление переменных

		for (x = 0; x < 3; x++) {
			for (y = 0; y < 3; y++) {
				a[x][y] = x + y;
				b[y][x] = y + x; //заполнение массива
			}

		}
		for (x = 0; x < 3; x++)

		{
			cout << '\n';
			for (y = 0; y < 3; y++) {
				c[x][y] = a[x][y] * b[y][x]; //поэлементное умножение
				cout << c[x][y] << " ";
			}
		}
	}
	if (num == 2) {
		int m1[3][3] = { { 1,2,3 },{ 4,5,6 },{ 7,8,9 }, }; //объявление переменных
		int m2[3][3] = { { 4,5,6 },{ 5,3,1 },{ 3,6,7 }, };
		int m3[3][3] = { { 0 } };
		int m4[3][3] = { { 0 } };
		int a;
		int sum = 0;
		int vec[3] = { 3,5,7 };
		int b;
		int vec1[3] = { 5,6,3 };
		int vec2[3];

		for (int i = 0; i < 3; i++) { //заполнение массива
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {

					m4[i][j] += m1[i][k] * m2[k][j]; //умножение
				}
				cout << m4[i][j] << '\t';
			}
			cout << "\n";
		};

		cout << "\n";
		cout << "\n";
	}
	if (num == 3) {
		int v[3], ve[3] = { 0 };
		int i, j, a = 0;
		int b[3][3] = { 0 };

		for (i = 0; i < 3; i++) {
			v[i] = i;
			for (j = 0; j < 3; j++) {
				b[i][j] = i + j; //заполнение массива
			}
		}
		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				ve[i] = ve[i] + b[i][j] * v[j]; //умножение матрицы на вектор
			}
		}
		for (int i = 0; i < 3; ++i) {
			cout << '\n' << ve[i] << " ";//вывод вектора
		}

	}
	if (num == 4) {

		int v[3], ve[3] = { 0 };
		int i, j, a = 0; //объявление переменных

		for (i = 0; i < 3; i++) {
			v[i] = i;
			for (j = 0; j < 3; j++) {
				ve[i] += i + j; //заполнение массива
			}
		}
		for (i = 0; i < 3; i++) {
			a += v[i] * ve[i]; //умножение векторов
		}
		cout << a;
	}
	return 0;
}
