#include <iostream>
#include "LabClass.h"
/*
1. Библиотеки, подключаемые в виде исходных файлов

Как правило, файл с расширением *.h или *.cpp, либо пара файлов
 *.h и *.cpp с одинаковым названием. Содержат объявления и определения
 переменных, констант, функций и классов. Подключаются в приложение
 через #include заголовочного файла

 В *.h как правило приводится объявление сущностей, а в расскрывается
 определение (реализация). Такой подход повышает читаемость, структуированность
 кода, однако ничто не мешает реализации (определение) также поместить в *.h файл

 Преимущества:
1) Исходный код открыть
2) Его всегда можно модифицировать и исправить
3) Простота подключения

Недостаток:
1) При малейшем изменении в приложении перекомпелируется и
перестраивается весь проект, включая и библиотеку,
если она сама не менялась

2. СТАТИЧЕСКИЕ БИБЛИОТЕКИ
*.lib (англ. library) - файлы статически подключаемых библиотек
Недостаток прошлого подхода компенсируется, если библиотечный код предварительно
скомпилировать и построить, и уже скомпилированный бинарный файл
подключать в приложение

Преимущества:
1) Более быстрая пересборка проекта
2) Так как код из бибилиотеки встраивается в *.exe, он не потеряется

Недостатки:
1) Бинарники *.lib надр стачала найти или построить самим из исходников
2) Бинарная несовместимость: *.lib должны подходить под
	- платформу (ОС и окружение)
	- аппаратура (тип процессора и разрядность)
	- компилятор и т.д.
3) Затруднительность аудита безопасности бинарных файлов
по данной причине библиотеки, связанные с ИБ, шифрованием, защитой
распространяются ТОЛЬКО в виде исходников, из которых разработчик
на месте должен сначала построить *.lib самостоятельно
В *.nix системах аналог *.lib файла имеет расширение *.a
4) Размер exe файла увеличивается, так как код функции из *.lib
дописывается к exe

ПОДКЛЮЧЕНИЕ СТАТИЧЕСКОЙ БИБЛИОТЕКИ
1. При отсутствии бинарников исходники библиотеки собирать в
*.lib(настройки -> "Тип конфигурации")
2. В настройках проекта VS "Компоновщик" -> "Ввод"
добавить название файла библиотеки *.lib
3. В "Каталоги VC++" добавить путь к библиотеке
4. В исходниках вашего проекта объявить функцию со спецификатором extern
или добавить *.h файл библиотеки
Сведение приложения м подключенной статической библиотеки
производится уже после компиляции, на следующем этапе -
этапе сборки (linking)
Линкер ищет функцию по имени в *.lib- файле. Если находит -
берёт машинный код функции и дописывает к *.exe. Если  нет - возникает ошибка
LNK2019 или схожие

3. ДИНАМИЧЕСКИЕ БИБЛИОТЕКИ
*.dll (Dynamic Link Library) - файлы динамически подключаемых библиотек
*.so (sourse object) - динамические библиотеки в Linux
*/

using namespace std;
int main()
{
	LabClass ojb1;

}