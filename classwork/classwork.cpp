﻿#include "pch.h"
#include <iostream>

using namespace std;

void func() {
	class vehicle {
	private:
		double speed; //скорость
	public:
		vehicle(/*могут быть входные параметры*/)
			//конструктор, не возвращает параметров, обязательно публичный
		{
			std::cout << "constructor(vehicle)" << '\n';
			return;
		}

		~vehicle(/*НЕ могут быть*/) //деструктор
		{
			std::cout << "destructor(vehicle)" << '\n';
			return;
		}
		char
			regnumber[6]; //госномер
		unsigned int capacity; //число пассажиров

		bool check_regnumber()
		{
			const char * true_number = "K37OAO";
			for (int i = 0; i < 6; i++) {
				if (regnumber[i] != true_number[i]) {
					return false;
				}
			}
			return true;
		}
	};
	cout << "class created" << '\n';
	vehicle test;
	cout << "obj created" << '\n';
	test.capacity = 2;
	cout << test.capacity << '\n';
	cout << "func exit" << '\n';
}

int main()
{
	func();
	cout << "main func" << '\n';
	return 0;
}
